package com.example.project_1_statement

import android.annotation.SuppressLint
import android.os.SystemClock
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModel
import org.w3c.dom.Text

@SuppressLint("StaticFieldLeak")
class GameViewModel : ViewModel() {
    // Game data model
    var images = arrayOf(
        R.drawable.cyndaquil,
        R.drawable.totodile,
        R.drawable.chikorita,
        R.drawable.cyndaquil,
        R.drawable.totodile,
        R.drawable.chikorita
    )
    var cards = mutableListOf<Card>()
    // Timer
    var runningChrono: Boolean = false
    var pauseOffSet: Long = 0
    // Contador moviments
    var movesCount: Int = 0
    // Cartes girades
    var flippedCards = 0

    // Aquesta funció es estàndard de cada classe, i és com el constructor
    init {
        setDataModel()
    }

    // Funció que barreja les imatges de les cartes i crea l'array de Cartes
    private fun setDataModel() {
        images.shuffle()
        for (i in 0..5) {
            cards.add(Card(i, images[i]))
        }
    }

    // Funció que comprova l'estat de la carta, el canvia i envia
    // a la vista la imatge que s'ha de pintar
    fun girarCarta(idCarta: Int) : Int {
        if (!cards[idCarta].flipped) {
            cards[idCarta].flipped = true
            return cards[idCarta].front
        } else {
            cards[idCarta].flipped = false
            return R.drawable.logo_text
        }
    }

    // Funció que retorna l'estat actual d'una carta
    fun estatCarta(idCarta: Int): Int {
        if(cards[idCarta].flipped) {
            return cards[idCarta].front
        } else {
            return R.drawable.logo_text
        }
    }

    // Funció que retorna la transparencia d'una carta
    fun transparencyCarta(idCarta: Int, v: ImageView): Float {
        if(cards[idCarta].transparency == 0) {
            return 0.6F
        } else {
            return 1F
        }
    }
    
    // Funció que arrenca el timer
    fun startTimer(c: Chronometer) {
        if (!runningChrono) {
            c.base = SystemClock.elapsedRealtime() - pauseOffSet
            c.start()
            runningChrono = true
        }

    }
    // Funció que pausa el tiemr
    fun pauseTimer(c: Chronometer) {
        if (runningChrono) {
            c.stop()
            pauseOffSet = SystemClock.elapsedRealtime() - c.base
            runningChrono = false
        }
    }
    // Funció que retorna l'estat del chronometer
    fun saveTimerState(c: Chronometer): Chronometer {
        return c
    }

    // Funció que guarda els moviments fets
    @SuppressLint("SetTextI18n")
    fun moveCounter(i: Int, tv: TextView) {
        movesCount += i
        tv.text = "MOVES: $movesCount"
    }

    // Retorna l'estat dels moviments
    fun saveMoveState(): CharSequence {
        return "MOVES: $movesCount"
    }

    // Guarda l'estat del chronometer
    fun saveBaseState(chrono: Chronometer): Long {
        if (!runningChrono) {
            return chrono.base - pauseOffSet
        } else {
            chrono.start()
            return  chrono.base
        }
    }

    // Guarda l'estat del botó de play/pause
    fun pausePlayState(): Int {
        if (!runningChrono) return R.drawable.play_button
        else return R.drawable.pause_button
    }
}