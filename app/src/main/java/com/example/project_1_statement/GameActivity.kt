package com.example.project_1_statement

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Html
import android.view.View
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.ViewModelProvider
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo

class GameActivity : AppCompatActivity(), View.OnClickListener {
    // UI Views
    private lateinit var c1: ImageView
    private lateinit var c2: ImageView
    private lateinit var c3: ImageView
    private lateinit var c4: ImageView
    private lateinit var c5: ImageView
    private lateinit var c6: ImageView
    private var difficulty: String = "easy"
    private lateinit var moves: TextView
    private lateinit var pausePlay: ImageView
    private lateinit var chrono: Chronometer

    // ViewModel declaration
    private lateinit var viewModel: GameViewModel

    @SuppressLint("ResourceType", "CutPasteId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_screen)
        supportActionBar?.hide()

        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)

        pausePlay = findViewById(R.id.pause_button)
        moves = findViewById(R.id.moves)
        chrono = findViewById(R.id.timer)

        c1 = findViewById(R.id.card_1)
        c2 = findViewById(R.id.card_2)
        c3 = findViewById(R.id.card_3)
        c4 = findViewById(R.id.card_4)
        c5 = findViewById(R.id.card_5)
        c6 = findViewById(R.id.card_6)

        pausePlay.setOnClickListener(this)

        c1.setOnClickListener(this)
        c2.setOnClickListener(this)
        c3.setOnClickListener(this)
        c4.setOnClickListener(this)
        c5.setOnClickListener(this)
        c6.setOnClickListener(this)

        if (viewModel.pauseOffSet == 0L) {
            startTimer()
        }
        updateUI()

    }

    // Accions per cada cop que cliques una view o botó
    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onClick(v: View?) {
        var cards = viewModel.cards
        // Atura i arrenca el timer
        when (v) {
            pausePlay -> {
                if (viewModel.runningChrono) {
                    pauseTimer()
                    popDialog()
                } else {
                    startTimer()
                    moveCounter(-1)
                }
            }
        }
        // Si el timer està en marxa:
        if (viewModel.runningChrono) {
            when (v) {
                c1 -> {
                    if (!cards[0].matched)
                        flipCard(0, c1, cards[0])
                }
                c2 -> {
                    if (!cards[1].matched)
                        flipCard(1, c2, cards[1])
                }
                c3 -> {
                    if (!cards[2].matched)
                        flipCard(2, c3, cards[2])
                }
                c4 -> {
                    if (!cards[3].matched)
                        flipCard(3, c4, cards[3])
                }
                c5 -> {
                    if (!cards[4].matched)
                        flipCard(4, c5, cards[4])
                }
                c6 -> {
                    if (!cards[5].matched)
                        flipCard(5, c6, cards[5])
                }
            }
            // Suma moviment
            moveCounter(1)

        }
    }

    // Gira la carta y comprova si ha trobat una parella
    @SuppressLint("UseCompatLoadingForDrawables")
    fun flipCard(id: Int, c: ImageView, carta: Card) {
        if (!carta.matched) {
            carta.matched = true

            var cards = viewModel.cards
            YoYo.with(Techniques.FlipInY)
                .duration(700)
                .playOn(c)
            girarCarta(id, c)

            var pair = 0
            viewModel.flippedCards++
            c.handler.postDelayed({
                c.alpha = 0.6F
                for (card in cards) {
                    if (card.flipped && card.front == carta.front) { // Si troba la parella redueix l'opacitat de la parella
                        pair++
                        card.transparency = 0
                        carta.transparency = 0
                    }
                }
                if (pair != 2) {
                    pair--
                    viewModel.flippedCards--
                    carta.matched = false
                    c.alpha = 1F
                    for (card in cards) {
                        card.transparency = 1
                        carta.transparency = 1
                    }
                    YoYo.with(Techniques.FlipInY)
                        .duration(700)
                        .playOn(c)
                    girarCarta(id, c)
                }
            }, 1200)
        }
        if (viewModel.flippedCards == 6) {
            var intent: Intent = Intent(this, ScoreActivity::class.java)
            pauseTimer()
            intent.putExtra("difficulty", difficulty)
            intent.putExtra("timer", viewModel.pauseOffSet)
            intent.putExtra("moves", viewModel.movesCount)

            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(intent)
            }, 2000)
        }
    }

    // Obre un diàleg amb l'opció de continuar o tancar el joc
    private fun popDialog() {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("El joc està pausat:")
        dialog.setMessage("Si vols continuar jugant prem \"Continuar\", " +
                "d'altre banda si vols finalitzar el joc prem \"Tancar\"")
        dialog.setPositiveButton(Html.fromHtml("<font color='#23D062'>" + "Continuar" + "</font>")) { _: DialogInterface, _: Int ->
            startTimer()
        }
        dialog.setNegativeButton(Html.fromHtml("<font color='#FF735A'>" + "Tancar" + "</font>")) { _: DialogInterface, _: Int ->
            stopGame()
        }
        dialog.show()
    }

    //Torna el menu
    private fun stopGame() {
        val menu = Intent(this, MainActivity::class.java)
        startActivity(menu)
    }

    // Pausa el timer
    private fun pauseTimer() {
        viewModel.pauseTimer(chrono)
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            println("dark theme")
            pausePlay.setImageResource(R.drawable.play_night)
        } else {
            println("light theme")
            pausePlay.setImageResource(R.drawable.play_button)
        }
    }

    // Comença el timer
    private fun startTimer() {
        viewModel.startTimer(chrono)
        pausePlay.setImageResource(R.drawable.pause_button)
    }

    // Contador moviments
    private fun moveCounter(i: Int) {
        viewModel.moveCounter(i, moves)
    }

    // Funció que utilitzarem per girar la carta
    private fun girarCarta(idCarta: Int, carta: ImageView) {
        carta.setImageResource(viewModel.girarCarta(idCarta))
    }

    // Funció que restauarà l'estat de la UI
    private fun updateUI() {
        c1.setImageResource(viewModel.estatCarta(0))
        c2.setImageResource(viewModel.estatCarta(1))
        c3.setImageResource(viewModel.estatCarta(2))
        c4.setImageResource(viewModel.estatCarta(3))
        c5.setImageResource(viewModel.estatCarta(4))
        c6.setImageResource(viewModel.estatCarta(5))
        c1.alpha = viewModel.transparencyCarta(0, c1)
        c2.alpha = viewModel.transparencyCarta(1, c2)
        c3.alpha = viewModel.transparencyCarta(2, c3)
        c4.alpha = viewModel.transparencyCarta(3, c4)
        c5.alpha = viewModel.transparencyCarta(4, c5)
        c6.alpha = viewModel.transparencyCarta(5, c6)
        pausePlay.setImageResource(viewModel.pausePlayState())
        moves.text = viewModel.saveMoveState()
        chrono = viewModel.saveTimerState(chrono)
        chrono.base = viewModel.saveBaseState(chrono)
    }
}