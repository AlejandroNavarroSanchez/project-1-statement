package com.example.project_1_statement

data class Card(
    var id: Int,
    var front: Int,
    var back: Int = R.drawable.logo_text,
    var flipped: Boolean = false,
    var matched: Boolean = false,
    var transparency: Int = 1)
