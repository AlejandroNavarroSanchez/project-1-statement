package com.example.project_1_statement

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class ScoreActivity : AppCompatActivity() {
        private lateinit var menuBtn: Button
        private lateinit var playAgainBtn: Button
        private lateinit var scoreTxt: TextView

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.score_screen)
        supportActionBar?.hide()

        val extras = intent.extras
        val gameMode = extras?.getString("difficulty")
        val gameTimer = extras?.getLong("timer")
        val gameMoves = extras?.getInt("moves")

        val moves: Int? = gameMoves?.plus(1)
        val time: Double = gameTimer?.div(1000)!!.toDouble()

        val score: Int

        menuBtn = findViewById(R.id.menu_button)
        playAgainBtn = findViewById(R.id.play_again_button)
        scoreTxt = findViewById(R.id.score)

        var movePoints = 0
        if (gameMode == "easy") {
            movePoints = when (moves) {
                in 0..6 -> 1000
                in 7..9 -> 750
                in 10..12 -> 450
                in 13..15 -> 175
                in 16..18 -> 50
                else -> 0
            }
        } else if (gameMode == "hard" || gameMode == "insane") {
            movePoints = when (moves) {
                in 0..9 -> 1000
                in 10..12 -> 750
                in 13..16 -> 450
                in 17..20 -> 175
                in 21..24 -> 50
                else -> 0
            }
        }

        val timePoints: Int = when (time) {
            in 0.0..4.9 -> 800
            in 5.0..6.9 -> 675
            in 7.0..9.9 -> 450
            in 10.0..14.9 -> 250
            in 15.0..19.9 -> 125
            in 20.0..29.9 -> 50
            else -> 0
        }

        val gameModePoints:Int = when (gameMode) {
            "easy" -> 100
            "hard" -> 300
            "insane" -> 500
            else -> 0
        }

        score = movePoints.plus(timePoints).plus(gameModePoints)

        scoreTxt.text = "SCORE\n\n" +
                "\t- Movement: $movePoints\n\n" +
                "\t- Time: $timePoints\n\n" +
                "\t- ${gameMode?.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }} mode: $gameModePoints\n\n\n" +
                "TOTAL SCORE: $score"

        menuBtn.setOnClickListener {
            val menu: Intent = Intent(this, MainActivity::class.java)
            startActivity(menu)
        }
        playAgainBtn.setOnClickListener {
            when (gameMode) {
                "easy" -> {
                    val easyMode: Intent = Intent(this, GameActivity::class.java)
                    startActivity(easyMode)
                }
                "hard" -> {
                    val hardMode: Intent = Intent(this, HardGameActivity::class.java)
                    startActivity(hardMode)
                }
                "insane" -> {
                    val insaneMode: Intent = Intent(this, InsaneGameActivity::class.java)
                    startActivity(insaneMode)
                }
            }
        }
    }
}