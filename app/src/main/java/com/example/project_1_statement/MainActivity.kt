package com.example.project_1_statement

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate

class MainActivity : AppCompatActivity() {
    // Variables
    lateinit var play_button: Button
    lateinit var help_button: Button
    lateinit var diff_spinner: Spinner

    @RequiresApi(Build.VERSION_CODES.R)
    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.SplasTheme)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu_screen)

        // Declaració variables
        play_button = findViewById(R.id.play_button)
        help_button = findViewById(R.id.help_button)
        diff_spinner = findViewById(R.id.difficulty_spinner)

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.NightMode) //when dark mode is enabled, we use the dark theme
        } else {
            setTheme(R.style.Theme_Project_1_Statement)  //default app theme
        }

        diff_spinner.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.difficulty,
            R.layout.style_spinner
        )

        play_button.setOnClickListener {
            if (diff_spinner.selectedItemPosition == 0) {
                val intent1: Intent = Intent(this, GameActivity::class.java)
                startActivity(intent1)
            } else if (diff_spinner.selectedItemPosition == 1) {
                val intent2: Intent = Intent(this, HardGameActivity::class.java)
                startActivity(intent2)
            } else if (diff_spinner.selectedItemPosition == 2) {
                val intent3: Intent = Intent(this, InsaneGameActivity::class.java)
                startActivity(intent3)
            }
        }

        help_button.setOnClickListener {
            showHelpDialog()
        }
    }
    fun showHelpDialog() {
        var dialog = AlertDialog.Builder(this)
        dialog.setTitle("Instruccions:")
        dialog.setMessage("" +
                "Easy:\n\t- 6 cartes. (Parelles)\n" +
                "Hard:\n\t- 8 cartes. (Parelles)\n" +
                "Insane:\n\t- 9 cartes. (Trios)\n\n" +
                "Has de trobar la parella o trio de cada carta per finalitzar el joc.\n" +
                "En funció a la quantitat de cartes que facis girar y del temps que portis, la puntuació serà major o menor.")
        dialog.setPositiveButton(Html.fromHtml("<font color='#23D062'>" + "Aceptar" + "</font>")) { dialogInterface: DialogInterface, i: Int -> }
        dialog.show()

    }
}